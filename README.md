# Gulp Email Assemble

Designing and testing emails should be fun. You don't need to spend all your time inlining css or worrying about what will and won't work. You need to be creating great experiences. Email Assemble helps you do just that.

Email Assemble does a few things for you

1. Compiles your SCSS to CSS

2. Builds your emails HTML from templates and components

3. Inlines your CSS

4. Compresses your images

5. Gives you helpers for; background images, responsive design and buttons (that all work in Outlook)

6. Uploads your images to a CDN (optional)

7. Makes you fee a little tingly (optional)

## Requirements

You'll need a few bits and pieces to get started, if you haven't already installed them.

* Node.js(Latest LTS recommended)
* Gulp-cli (`npm install --global gulp-cli`)

## Getting started

#### 1. Setup

Clone this repository, cd to the directory and run either 'npm install' or the package manager of your choice. Then go and make yourself a beverage (optional) while the packages are installed.

#### Generate your email templates

In Terminal/command-line, run 'gulp' to build your emails.
This command also:
* Cleans your build directory
* Compresses all images

#### Browser preview and live editing

In Terminal/command-line, run 'gulp serve' to run and open the email preview.

This view will give you a way to access all your templates and preview them at both desktop and mobile sizes. It will also give you live code updates so as you make changes to your html, templates, css or data the changes will be reflected almost immediately in the browser.

This command watches for changes to:
* Email templates
* Email data
* images

#### Amazon S3 Image serving

In Terminal/command-line, run 'gulp s3upload' after your, emails have been built, to upload all images to Amazon S3. This requires a valid *aws.json* file in the root directory. A example can be found in this repository.

#### 2. Directory Structure

Source files for all emails are contained in the *src* directory.

Within the directory are two main sub folders
* emails
* shared

###### Shared Folder
All files contained in shared are made available to every email created inside the email folder. This means that regularly used styles or components can be kept away from your email builds (handy).

###### Emails Folder
The emails folder allows you to create groups of emails by campaign, or theme. Each folder beneath email will be treated as a separate project. With it's own data, images, styles and templates.

Inside the a templates folder you will find by default
* email
* layouts
* partials

The files inside the email folder represent the list of files that will be output for this template.
